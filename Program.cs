﻿using System;
using System.Text;
using System.Linq;
using System.Runtime.Intrinsics.Arm;
using System.Security.Cryptography;

namespace eDocSignature
{
    partial class Program
    {
        static void Main(string[] args)
        {
            string text = "payload";
            string passphrase = "secret";
            var encoded = Crypt.OpenSSLEncrypt(text, passphrase);
            Console.WriteLine($"encoded:{encoded}");
            var result = Crypt.OpenSSLDecrypt(encoded, passphrase);
            Console.WriteLine($"result:{result}");
        }

        static string Login() =>
            DoRIPActionRequest(Config.RipAddress + "/SESSIONS", "POST", new {
                user = Config.User,
                passwordhash = GetHash(Config.Password),
                host = GetClientIp(),
                controlid = Config.ControlId
            });

        static string Logout(string Session) =>
            DoRIPActionRequest(Config.RipAddress + "/SESSIONS", "DELETE", new {
                session = Session,
                host = GetClientIp(),
                controlid = Config.ControlId
            });

        static string Handoff() =>
            DoRIPActionRequest(Config.RipAddress + "/SESSIONS", "POST", new {
                edocsig = "", //TODO
                action = "EDOCSIG",
                host = GetClientIp(),
                controlid = Config.ControlId
            });

        static string CreateDfc()
        {
            var signer = new {
                id = "Signer1",
                name = "Joe Johnson",
                email = "jjohnson@mycu.com"
            };

            return DoRIPActionRequest(Config.RipAddress + "/DFC", "POST", new {
                handoff = "", //TODO
                action = "EDOCSIG",
                host = GetClientIp(),
                controlid = Config.ControlId,
                formname = "",
                pkgname = "",
                pkgid = "",
                signatureboxes = new[]
                {
                    //First box
                    new {
                        id = "Box1",
                        xp = "4.8202614379084965",
                        yp = "69.065656565656568",
                        wp = "2.6143790849673203",
                        hp = "1.957070707070707",
                        page = "0",
                        type = "4",
                        fieldname = "Chaperone",
                        checkedvalue = "Y",
                        uncheckedvalue = "N",
                        signer = signer,
                        required = ""
                    },
                    new {
                        id = "Box2",
                        xp = "17.075163398692812",
                        yp = "84.343434343434339",
                        wp = "42.483660130718953",
                        hp = "2.2095959595959598",
                        page = "0",
                        type = "3",
                        fieldname = "FullName",
                        checkedvalue = "",
                        uncheckedvalue = "",
                        signer = signer,
                        required = "1"
                    },
                    new {
                        id = "Box3",
                        xp = "19.607843137254903",
                        yp = "91.856060606060609",
                        wp = "39.787581699346404",
                        hp = "2.3358585858585861",
                        page = "0",
                        type = "0",
                        fieldname = "",
                        checkedvalue = "",
                        uncheckedvalue = "",
                        signer = signer,
                        required = "1"
                    }
                },
                files = ""
            });
        }
    }
}
