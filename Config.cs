namespace eDocSignature
{
    public class Config
    {
        public static string RipAddress { get; } = "https://test";
        public static string ControlId { get; } = "testConrolId";
        public static string User { get; } = "testUser";
        public static string Password { get; } = "testPassword";
        public static string HexKey { get; } = "ABCDEF1010101010ABCDEF1010101010ABCDEF1010101010";
    }
}
