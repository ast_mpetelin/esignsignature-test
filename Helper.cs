using System;
using System.Net;
using System.Linq;
using Newtonsoft.Json;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace eDocSignature
{
    partial class Program
    {
        static string bin2hex(string input) =>
            string.Join("", input.ToCharArray().Select(c => $"{Convert.ToInt32(c):X}"));

        static string hex2bin(string hex)
        {
            byte[] raw = new byte[hex.Length / 2];
            for (int i = 0; i < raw.Length; i++)
                raw[i] = Convert.ToByte(hex.Substring(i * 2, 2), 16);
            return Encoding.ASCII.GetString(raw);
        }

        public static string getRandomString(int length = 8) =>
            new string(Enumerable.Range(0, length).Select(i => "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"[new Random().Next(0, 36)]).ToArray());

        static string GetHash(string input) =>
            Encoding.ASCII.GetString(SHA1.Create().ComputeHash(Encoding.ASCII.GetBytes(input)));

        static string GetClientIp() =>
            Dns.GetHostEntry(Dns.GetHostName()).AddressList
                .FirstOrDefault(i => i.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)?.ToString();

        static string DoRIPActionRequest(string url, string method, object data)
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = method;

            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                string json = JsonConvert.SerializeObject(data);
                streamWriter.Write(json);
            }

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                return streamReader.ReadToEnd();
            }
        }
    }
}
